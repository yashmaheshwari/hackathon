## Event summary

(Enter dates for the Hackathon plus any relevant notes for the event such as new themes, prizes, etc.) 

## Prior to the Hackathon

### Hackathon minus 4 weeks
1. [ ] Update the Hackathon page(s) with all the necessary information
    1. [ ] Update the dates
    2. [ ] Update the counter
    3. [ ] Add the new Hackathon to the [GitLab events page](https://about.gitlab.com/events/) as a "featured" event.
    4. [ ] Move the previous Hackathon content to the [Past Events page](https://about.gitlab.com/community/hackathon/#past-events). 
2. [ ] Open an issue in [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing) to [request a social promotion](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-promotion) on Facebook, LinkedIn, and Twitter: An example from the [Q1 Hackathon](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/1686)
3. [ ] Make an announcement on following channels:
   1. [ ] [Forum](https://forum.gitlab.com/). Consider making the topic a banner on the forum for the period leading into the event. 
   2. [ ] [Gitter](https://gitter.im/gitlabhq/community)
   3. [ ] [Reddit](https://www.reddit.com/r/gitlab/)
4. [ ] [Open an issue](https://gitlab.com/gitlab-com/internal-communications/newsletter/-/issues) for including the Hackathon in the upcoming montlhy newsletter. 
5. [ ] Submit a request for the GitLab bot to auto-add the Hackathon label to any community MRs during the Hackathon period
6. [ ] Open an issue to gather input from various teams in regard to important issues for people to contribute to. [issue](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/59)
    1. [ ] Post at #what-happening-at-gitlab inviting people to add issues
    2. [ ] #tw-team
    3. [ ] #community-relations 
    4. [ ] #eng-week-in-review
7. Update TAMs bi-weekly call agenda with the new Hackathon details and how customers can participate ([agenda doc](https://docs.google.com/document/d/1uaUS5vYn_qYGOOBWWGx4oKiXn0925YWO99R73V872J4/edit#))
8. [ ] Add the event in the [public calendar](https://calendar.google.com/calendar/u/0?cid=Y182YTNta2FiZzllcWlxNzVnYmY4bjQ4ZWlrY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
9. [ ] Start working with merchandise vendors (e.g. Printfection) to select and order Hackathon prizes.   
    - Merged MR: Prize for anyone with an MR merged. ($10-$15 range/qty: 40-50)
    - Second place: Anyone with more than 20 MRs (but less than 40 MRs) merged. ($50 range/qty:3-5)
    - Grand prize: Anyone with more than 40 MRs merged. The prize will be a a $100 coupon on shop.gitlab.com, so no merchdise needs to be ordered.
10. [ ] Open an issue for tracking community contributions during the Hackathon. [An example from Q1'20](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/31)

### Hackathon minus 2-3 weeks
1.  [ ] Line-up topics and speakers for tutorial sessions.
3.  [ ] Post reminders on Forum/Gitter/Reddit.
4.  [ ] Create calendar events with Zoom details for each scheduled session
5.  [ ] Update the Hackathon website with the session details 
6.  [ ] Update the GitLab Public Calendar with event details 
  
### Hackathon minus 1 week
1.  [ ] Post reminders on:
   1. [ ] [Forum](https://forum.gitlab.com/)
   2. [ ] [Gitter](https://gitter.im/gitlabhq/community)
   3. [ ] [Reddit](https://www.reddit.com/r/gitlab/)
   4. [ ] Slack


## During the Hackathon
1.  [ ] Announce the event kickoff on Forum/Gitter/Reddit.
2.  [ ] Post recorded tutorial sessions after each session 
3.  [ ] MR's
    1. [ ] Add the submitted MR's to the issue create above to track MR's. 
    2. [ ] Ensure that the MR counter on the landing page is working, once the first MR has been submitted.
    3. [ ] Triage the MR's by applying labels and mentioning appropriate product managers/groups for the MR.
4.  [ ] Throughout the event, monitor Gitter and other channels for questions/discussions. If necessary escalate any questions to other GitLab team members (e.g. #mr-coaching on Slack).
5.  [ ] At the end of Day 1, post a wrap-up tweet with accomplishments highlighted.
6.  [ ] At the end of the event, post an event wrap-up tweet and thank the community.

## After the Hackathon
1.  [ ] Post an event wrap-up on Forum/Gitter/Reddit channels.
2.  [ ] Post a thank you message on #thanks Slack channel. 
3.  [ ] Select the next Hackathon dates and update the dates and countdown clock on the [GitLab Hackathon page](https://about.gitlab.com/community/hackathon/). The new hackthon dates should be set [in the code](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/javascripts/hackathon.js#L4). 
4.  [ ] Update the Hackathon banner with the next Hackathon date on the [Contribute page](https://about.gitlab.com/community/contribute/).
5.  [ ] Give a remider to the community on the deadline for merged MR's and who they can contact if they need any assistance with MR's. 
6.  [ ] Post a wrap-up post on the [forum](https://forum.gitlab.com).
7.  [ ] Update the [GitLab Hackathon page](https://about.gitlab.com/community/hackathon/) with links to prize winners and the wrap-up blog post. 
